import React, { Fragment, useState } from 'react'

const EditTodoItem = ({ id, editTodo, changeEditable }) => {
  const [value, setValue] = useState('');

  const onChange = (e) => {
    setValue(e.target.value);
  }

  const onSubmit = (e) => {
    e.preventDefault();
    editTodo(id, value);
    changeEditable();
    setValue(' ');
  }

  return (
    <Fragment>
      <form className="d-flex ml-auto" onSubmit={onSubmit}>
        <input 
          type="text"
          value={value} 
          className="order-2 mr-2"
          onChange={onChange}
        />
        <input 
          type="submit" 
          value="Submit" 
          className="order-1 mr-2"
          style={editBtn}
        />
      </form>
    </Fragment>
  )
}

const editBtn = {
  padding: '0 10px',
  background: '#343a40',
  borderRadius: '5px',
  border: 'none',
  color: '#efefef'
}

export default EditTodoItem;
