import React, { useState } from 'react'
import EditTodoItem from './EditTodoItem'

const TodoItem = ({ todo, delTodo, editTodo }) => {
  const [editable, setEditable] = useState(false);

  const {id, title} = todo;
  
  const onClick = () => {
    delTodo(id);
  }

  const editClick = () => {
    setEditable(!editable);
  }

  const changeEditable = () => {
    setEditable(false);
  }
  
  return (
    <div className="bg-light px-4 py-3 d-flex" style={todoItem}>
      <p className="m-0"> {title} </p>
      <div className="ml-auto d-flex">
        {
          editable && <EditTodoItem id={id} editTodo={editTodo} changeEditable={changeEditable} />
        }
        <button 
          type="button" 
          className="btn btn-sm btn-success"
          onClick={editClick}
        >
          edit
        </button>
        <button 
          type="button" 
          className="btn btn-sm btn-danger ml-2"
          onClick={onClick}
        >
          X
        </button>
      </div>
    </div>
  )
}

const todoItem = {
  borderBottom: '1px dashed #ccc'
}

export default TodoItem;
