import React, { useState } from 'react'

const AddTodo = ({ addTodo }) => {
  const [value, setValue] = useState('');

  const onChange = (e) => {
    setValue(e.target.value);
  }

  const onSubmit = (e) => {
    e.preventDefault();
    addTodo(value);
    setValue(' ');
  }

  return (
    <div>
      <form style={formStyle} onSubmit={onSubmit}>
        <input 
          type="text" 
          placeholder="Add new Todo Item..."
          value={value}
          style={formInput}
          onChange={onChange}
        />
        <input 
          type="submit" 
          value="Submit"
          style={formBtn}
          className="btn-info text-light"
        />
      </form>
    </div>
  )
}

const formStyle = {
  display: 'flex'
}

const formInput = {
  padding: '5px',
  flex: '8'
}

const formBtn = {
  padding: '5px',
  flex: '2',
  border: 'none',
  outline: 'none'
}

export default AddTodo;
