import React from 'react'
import TodoItem from './TodoItem'

const Todos = ({ todos, delTodo, editTodo }) => {
  return todos.map(todo => (
    <TodoItem key={todo.id} todo={todo} delTodo={delTodo} editTodo={editTodo}/>
  ));
}

export default Todos;
