import React from 'react'

const Header = () => {
  return (
    <div className="bg-dark text-light pt-3 p-2 text-center">
      <h1> Simple Todo App</h1>
      <p> ZTI 2020 </p>
    </div>
  )
}

export default Header;
