import React, { useState, useEffect } from 'react';
import './App.css';
import axios from 'axios'
import Header from './components/Header'
import Todos from './components/Todos'
import AddTodo from './components/AddTodo'

const App = () => {
  const [todos, setTodos] = useState([]);
  const [reload, setReload] = useState(true);

  useEffect(() => {
    // get data from API
    axios.get('http://localhost:8082/api/v1/todos')
      .then(res => {
        console.log(res.data);
        setTodos(res.data);
      })
      .catch(e => console.log(e));
    setReload(false);
  }, [reload]);
  
  // add Todo to db by api
  const addTodo = (title) => {
    axios.post('http://localhost:8082/api/v1/todos', {
      title: title
    })
      .then(res => console.log(res));
    setReload(true);
  }

  // del Todo
  const delTodo = (id) => {
    axios.delete(`http://localhost:8082/api/v1/todos/${id}`)
      .then(res => console.log(res));
    setReload(true);
  }

  // edit Todo
  const editTodo = (id, value) => {
    axios.put(`http://localhost:8082/api/v1/todos/${id}`, {
      title: value 
    })
      .then(res => console.log(res));
    setReload(true);
  }

  return (
    <div className="App">
      <div className="container px-5">
        <Header />
        <AddTodo addTodo={addTodo} />
        <Todos todos={todos} delTodo={delTodo} editTodo={editTodo} />
      </div>
    </div>
  );
}

export default App;
